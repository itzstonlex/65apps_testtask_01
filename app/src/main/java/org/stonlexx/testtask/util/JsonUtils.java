package org.stonlexx.testtask.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

@UtilityClass
public class JsonUtils {

    public final Gson GSON               = new Gson();
    public final JsonParser JSON_PARSER  = new JsonParser();

    // inside class for public implementation.
    public interface Jsonable {

        default String toJson() {
            return JsonUtils.toJson(this);
        }
    }


    /**
     * Преобразование json строки в
     * дата-элемент
     *
     * @param json - строка формата json
     */
    public JsonElement parseToElement(@NonNull String json) {
        return JSON_PARSER.parse(json);
    }

    /**
     * Преобразовать объект в JSON
     *
     * @param object - объект
     */
    public String toJson(Object object) {
        return GSON.toJson(object);
    }

    /**
     * Преобразовать JSON обратно в объект
     *
     * @param json - JSON
     * @param clazz - класс объекта
     */
    public <T> T toObject(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

}
