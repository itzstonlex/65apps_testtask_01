package org.stonlexx.testtask.type;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.stonlexx.testtask.R;
import org.stonlexx.testtask.util.JsonUtils;
import org.stonlexx.testtask.util.TimeUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@FieldDefaults(level = AccessLevel.PUBLIC)
public class WorkerUser implements JsonUtils.Jsonable {

    // User names.
    String f_name;
    String l_name;

    // User meta data.
    String birthday;
    String avatr_url;

    // User specialties.
    List<Specialty> specialty;


    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    public static class UserListAdapter extends ArrayAdapter<WorkerUser> {

        TIntObjectMap<View> userViewsMap = new TIntObjectHashMap<>();

        public UserListAdapter(@NonNull Context context, Set<WorkerUser> workerUsers) {
            super(context, R.layout.user_item, new ArrayList<>(workerUsers));
        }

        @SuppressLint({"ResourceType", "ViewHolder"})
        @RequiresApi(api = Build.VERSION_CODES.R)
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            // Check cached view by current position.
            if (userViewsMap.containsKey(position)) {
                return userViewsMap.get(position);
            }

            // Else then create and cache new view.
            WorkerUser workerUser = getItem(position);
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_item, parent, false);

            // Get layout views.
            ImageView imageView = convertView.findViewById(R.id.user_avatar);

            TextView firstName = convertView.findViewById(R.id.first_name);
            TextView lastName = convertView.findViewById(R.id.last_name);
            TextView birthday = convertView.findViewById(R.id.birthday);
            TextView speciality = convertView.findViewById(R.id.specialty);

            // Init user data.
            new DownloadImageTask(imageView).execute(workerUser.avatr_url);

            firstName.setText(workerUser.f_name);
            lastName.setText(workerUser.l_name);

            speciality.setText(workerUser.specialty.stream().map(s -> s.name).collect(Collectors.joining(", ")));


            boolean availableBirthday = workerUser.birthday != null && !workerUser.birthday.isEmpty();

            if (availableBirthday) {
                Date originalDate = TimeUtils.parseDate("yyyy-MM-dd", workerUser.birthday);

                birthday.setTextColor(Color.DKGRAY);
                birthday.setText(TimeUtils.formatTime(originalDate.getTime(), "dd.MM.yyyy г."));

            } else {

                birthday.setTextColor(Color.RED);
                birthday.setText("-----");
            }

            userViewsMap.put(position, convertView);
            return convertView;
        }
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        @SuppressLint("StaticFieldLeak")
        ImageView imageView;

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                InputStream in = new java.net.URL(params[0]).openStream();
                return BitmapFactory.decodeStream(in);
            }
            catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            if (result != null) {
                imageView.setImageBitmap(result);
            }
        }
    }

}
