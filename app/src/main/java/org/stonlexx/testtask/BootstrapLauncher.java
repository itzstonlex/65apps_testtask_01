package org.stonlexx.testtask;


import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import org.jsoup.Jsoup;
import org.stonlexx.testtask.sqlite.WrappedSqliteDatabase;
import org.stonlexx.testtask.type.Specialty;
import org.stonlexx.testtask.type.WorkerUser;
import org.stonlexx.testtask.util.JsonUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.NonFinal;
import lombok.experimental.UtilityClass;

@RequiresApi(api = Build.VERSION_CODES.O)
public final class BootstrapLauncher extends AppCompatActivity {

// =============================================================================================================================== //

    public static final String SQLITE_TABLE                 = "WorkerUsers";
    public static final String HTTP_USERS_STORAGE_URL       = "https://gitlab.65apps.com/65gb/static/raw/master/testTask.json";

    public static final Path USERS_FOLDER                   = Paths.get(System.getProperty("java.io.tmpdir"));

// =============================================================================================================================== //

    /**
     * Обработчик подключения к база-серверу,
     * где и хранятся все пользователи в
     * виде JSON конфигурации.
     */
    @SuppressWarnings("all")
    private final class HttpJsonReaderTask extends AsyncTask<String, String, String> {

        private String formatUsername(@NonNull String name) {
            return Character.toUpperCase(name.charAt(0)) + name.substring(1).toLowerCase();
        }

        private void handleJsonArray(@NonNull JsonArray jsonArray,
                                     @NonNull Set<WorkerUser> workerUsers, @NonNull Set<Specialty> specialties) {

            for (JsonElement jsonElement : jsonArray) {
                WorkerUser workerUser = JsonUtils.toObject(jsonElement.toString(), WorkerUser.class);
                
                workerUser.f_name = formatUsername(workerUser.f_name);
                workerUser.l_name = formatUsername(workerUser.l_name);

                // Add user to collection.
                workerUsers.add(workerUser);

                // Add specialities to collection.
                for (Specialty userSpeciality : workerUser.specialty) {

                    if (specialties.stream().noneMatch(specialty -> specialty.specialty_id == userSpeciality.specialty_id)) {
                        specialties.add(userSpeciality);
                    }
                }

                // Debug logging.
                Log.d("[TEST -> WorkerUser]", workerUser.toJson());
            }
        }

        @Override
        @SneakyThrows
        protected String doInBackground(@NonNull String... params) {
            return Jsoup.connect(params[0]).get().text();
        }

        @Override
        protected void onPostExecute(@NonNull String result) {

            // Parse response to users list.
            JsonArray jsonArray = JsonUtils.parseToElement(result)
                    .getAsJsonObject()
                    .getAsJsonArray("response");

            Set<WorkerUser> workerUsers = new HashSet<>();
            Set<Specialty> specialties = new HashSet<>();

            handleJsonArray(jsonArray, workerUsers, specialties);


            // Draw users to application view.
            drawUsersOnApplicationView(workerUsers);

            // Draw specialities to application view.
            drawSpecialtyButtonsOnApplicationView(workerUsers, specialties);


            // Save users to sqlite database.
            sqliteDatabase.execute(String.format("DELETE FROM `%s`", SQLITE_TABLE));

            workerUsers.forEach(workerUser -> {
                sqliteDatabase.execute(String.format("INSERT INTO `%s` (`Json`) VALUES ('%s')", SQLITE_TABLE, workerUser.toJson()));
            });
        }
    }


    @Getter(AccessLevel.PRIVATE)
    private WrappedSqliteDatabase sqliteDatabase;

    /**
     * Создание подключения к базе
     * данных Sqlite
     *
     * @param table - таблица, к которой подключаемся,
     *              и в случае чего создаем
     */
    @RequiresApi(api = Build.VERSION_CODES.O_MR1)
    @SneakyThrows
    public void makeDatabaseConnection(String table) {

        // Create sqlite file.
        Path sqlitePath = USERS_FOLDER.resolve("sqlite.db");

        if (!Files.exists(sqlitePath))
            Files.createFile(sqlitePath);

        // Create sqlite connection.
        sqliteDatabase = new WrappedSqliteDatabase(SQLiteDatabase.openDatabase(sqlitePath.toFile(),
                new SQLiteDatabase.OpenParams.Builder().build()));

        if (!sqliteDatabase.isOpen()) {
            Log.w("[SQL]", "No Sqlite Connection!");
            return;
        }

        // Create and get the table.
        sqliteDatabase.execute(String.format("CREATE TABLE IF NOT EXISTS `%s` (`Json` LONGTEXT NOT NULL)", table));
    }

    // Вывел сюда для установки статичных параметров.
    @NonFinal ListView workerUsersView;

    /**
     * Отрисовка списка пользователей
     * на окне приложения
     *
     * @param workerUsers - список отрисовываемых
     *                    пользователей.
     */
    public void drawUsersOnApplicationView(@NonNull Set<WorkerUser> workerUsers) {
        WorkerUser.UserListAdapter userListAdapter = new WorkerUser.UserListAdapter(this, workerUsers);
        workerUsersView.setAdapter(userListAdapter);
    }

    /**
     * Отрисовать все кнопки специальностей
     * на окне приложения
     *
     * @param workerUsers - общий список всех пользователей
     *                    (для фильтрации списка под категории)
     * @param specialties - общий список всех специальностей
     */
    @SuppressWarnings("all")
    public void drawSpecialtyButtonsOnApplicationView(@NonNull Set<WorkerUser> workerUsers, @NonNull Set<Specialty> specialties) {

        /**
         * Локальный класс фабрики кнопок
         * для очистки дублированного кода
         * в создании кнопок.
         */
        class SpecialityButtonFactory {

            /**
             * Инициализация параметров кнопки.
             * 
             * @param button - кнопка, которую инициализируем.
             * @param text   - отображаемый текст кнопки.
             * @param workerUsers - список пользователей, которая она будет отображать
             */
            private void init(@NonNull LinearLayout layout, @NonNull Button button, @NonNull String text, @NonNull Set<WorkerUser> workerUsers) {
                button.setActivated(true);
                button.setClickable(true);

                button.setBackground(layout.getBackground());

                button.setWidth(350);

                button.setTextSize(18);
                button.setText(text);
                button.setTextColor(Color.WHITE);

                button.setOnClickListener(view -> drawUsersOnApplicationView(workerUsers));
            }

            /**
             * Создаем и инициализируем кнопку
             * @return - кнопка для сортировки специализации..
             */
            public Button createButton(@NonNull LinearLayout layout, @NonNull String text, @NonNull Set<WorkerUser> workerUsers) {
                Button button = new Button(BootstrapLauncher.this);
                init(layout, button, text, workerUsers);
                
                return button;
            }
        }

        LinearLayout specialitiesLayout = findViewById(R.id.specialty_layout);
        SpecialityButtonFactory buttonFactory = new SpecialityButtonFactory();

        // Add button for all specialities.
        specialitiesLayout.addView(buttonFactory.createButton(specialitiesLayout, "Все", workerUsers));

        // Add other specialties buttons.
        specialties.stream()

                // Таким образом я фильтрую по id, ибо Set не делает это за меня :/
                .mapToInt(specialty -> specialty.specialty_id)
                .distinct()

                // Затем преобразовываем обратно в объекты
                .mapToObj(id -> specialties.stream().filter(specialty -> specialty.specialty_id == id).findFirst().orElse(null))

                // ...и отрисовываем
                .forEach(specialty -> {

                    // Sort users list by current speciality.
                    Log.d("Button Sort", specialty.name);
                    Set<WorkerUser> usersBySpeciality = workerUsers.stream()
                            .filter(workerUser -> workerUser.specialty.stream().map(s -> s.specialty_id).anyMatch(id -> specialty.specialty_id == id))
                            .collect(Collectors.toSet());

                    // Create button.
                    Log.d("Button Create", specialty.name);
                    Button button = buttonFactory.createButton(specialitiesLayout, specialty.name, usersBySpeciality);

                    // Add button.
                    Log.d("Button Add", specialty.name);
                    specialitiesLayout.addView(button);
                });
    }


    @RequiresApi(api = Build.VERSION_CODES.O_MR1)
    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create sqlite-database connection.
        makeDatabaseConnection(SQLITE_TABLE);

        // Init users list view.
        workerUsersView = findViewById(R.id.worker_users_view);

        workerUsersView.getLayoutParams().height -= 80;
        workerUsersView.setY(50);


        // Connecting and handle worker-users from the http storage.
        HttpJsonReaderTask httpJsonReaderTask = new HttpJsonReaderTask();
        httpJsonReaderTask.execute(HTTP_USERS_STORAGE_URL);
    }

    @Override
    public void onStop() {
        sqliteDatabase.close();

        super.onStop();
    }

}