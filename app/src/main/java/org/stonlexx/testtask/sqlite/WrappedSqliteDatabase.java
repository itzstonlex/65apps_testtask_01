package org.stonlexx.testtask.sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import java.io.Closeable;
import java.util.Arrays;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public final class WrappedSqliteDatabase implements Closeable {

    @NonNull
    SQLiteDatabase sqliteDatabase;


    public SQLiteDatabase getHandle() {
        return sqliteDatabase;
    }

    public boolean isOpen() {
        return sqliteDatabase.isOpen();
    }

    /**
     * Выполнить SQL-запрос в базу
     *
     * @param sql - запрос.
     */
    public void execute(@NonNull String sql) {
        sqliteDatabase.execSQL(sql);
    }

    /**
     * Выполнить SQL-запрос в базу
     * с возможностью получения результата.
     *
     * @param sql           - запрос.
     * @param selectionArgs - выбранные аргументы запроса.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public Cursor executeWithCursor(@NonNull String sql, @Nullable Object... selectionArgs) {
        String[] sqlQueryArray = new String[0];

        if (selectionArgs != null) {
            sqlQueryArray = Arrays.stream(selectionArgs).map(Object::toString).toArray(i -> new String[0]);
        }

        return sqliteDatabase.rawQuery(sql, sqlQueryArray);
    }

    /**
     * Закрыть канал соединения с
     * сервером базы данных.
     */
    @Override
    public void close() {
        sqliteDatabase.close();
    }

}
